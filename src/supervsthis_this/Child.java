package supervsthis_this;

public class Child extends Parent {
	String childName;
	
	public Child() {
		this("ChildName");
//		childName = "ChildName";
	}
	
	public Child(String childName) {
		this.childName = childName;
		this.someMethod();
		someMethod();
		System.out.println(this);
	}
	
	void someMethod() {
		System.out.println("Hello!");
	}
}
