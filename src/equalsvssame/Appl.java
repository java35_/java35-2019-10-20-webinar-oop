package equalsvssame;

public class Appl {
	public static void main(String[] args) {
		
		Child child1 = new Child(1, "Name1", "Surname1", "Group1");
		Child child2 = new Child(1, "Name1", "Surname1", "Group1");
		
		System.out.println(child1 == child2);
		
		System.out.println(child1.equals(child2));
		
		Parent parent = new Parent(2, "name2", "surname2");
		
		System.out.println("******************");
		System.out.println(child1 instanceof Child);
		System.out.println(child1 instanceof Parent);
		System.out.println(child1 instanceof Object);
		
		parent = child1;
		System.out.println(parent instanceof Child);
		System.out.println(parent instanceof Parent);
		System.out.println(parent instanceof Object);
	}
}
