package equalsvssame;

public class Child extends Parent {
	String group;

	public Child(int id, String name, String surname, String group) {
		super(id, name, surname);
		this.group = group;
	}
	
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (!(obj instanceof Child))
			return false;
		
		Child tmpChild = (Child)obj;
		
		if (this.id == tmpChild.id)
			return true;
		
		return false;
	}
	
}
