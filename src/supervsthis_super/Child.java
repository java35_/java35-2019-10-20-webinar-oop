package supervsthis_super;

public class Child extends Parent {
	String parentName = "I am a child";
	String childName = "Child name";
	String valueChild;
	
	{
		valueChild = "Hi";
	}
	
	public Child(String parentName, String childName) {
		super("AAAA");
		this.parentName = parentName;
		this.childName = childName;
		
	}
	
	public void printParentName() {
		System.out.println(super.parentName);
	}
	
	@Override
	void printHello() {
		System.out.println("I am a child");
		super.printHello();
	}
}
