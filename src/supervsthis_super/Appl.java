package supervsthis_super;

public class Appl {
	public static void main(String[] args) {
		Parent parent = new Parent("Parent name");
		
		System.out.println(parent.parentName);
		
		Child child = new Child("Hello", "Child name");
		
		System.out.println(child.childName);
		System.out.println(child.parentName);
		
		child.printParentName();
		
		System.out.println("****************");
		parent.printHello();
		child.printHello();
		
		
		parent = child;
		
		// Object child -> Child
		// parent <- child
		
		System.out.println(parent.parentName);
		parent.printHello();
		
	}
}
