package supervsthis_super;

public class Parent {
	String parentName = "Name of parent";
	String value;
	{
		value = "Hi";
	}
	
	public Parent(String name) {
		parentName = name;
	}
	
	void printHello() {
		System.out.println("I am a parent");
	}
}
