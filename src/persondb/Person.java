package persondb;

import equalsvssame.Child;

public class Person {
	int id;
	String name;
	
	public Person(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (!(obj instanceof Person))
			return false;
		
		Person tmpPerson = (Person)obj;
		
		if (this.id == tmpPerson.id)
			return true;
		
		return false;
	}
}
