package persondb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PersonDB {

	public static void main(String[] args) throws IOException {
		DinamicArray persons = new DinamicArray();
		
		System.out.println("For input new person press 1");
		System.out.println(">");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String userAnswer = br.readLine();
		
		while (userAnswer.equals("1")) {
			System.out.println("Enter id: ");
			int id = Integer.parseInt(br.readLine());
			System.out.println("Enter name: ");
			String name = br.readLine();
			
			Person person = new Person(id, name);
			
			if (persons.contains(person)) {
				System.out.println("Person already exists. Input new person!");
				continue;
			}
			
			persons.add(person);
			
			System.out.println("For input new person press 1");
			System.out.println(">");
			userAnswer = br.readLine();
		}
	}
}
