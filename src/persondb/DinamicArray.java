package persondb;
public class DinamicArray implements IDinamicArray {

	private final static int INITIAL_CAPACITY 	= 10;
	private final static int EXSTENSIBILITY 	= 2;
	private int capacity;
	private int size;
	
	private Object[] elementData;
	
	public DinamicArray() {
		this(INITIAL_CAPACITY);
	}
	
	public DinamicArray(int capacity) {
		if (capacity <= 0)
			capacity = INITIAL_CAPACITY;
		elementData = new Object[capacity];
		this.capacity = capacity;
	}
	
	@Override
	public boolean add(Object obj) {
		if (obj == null)
			return false;
		
		if (capacity == size) {
			allocateMemory();
		}
		
		// Wrong!
//		elementData[++size] = obj;
		// size++;
		// elementData[size] = obj;
		
		elementData[size++] = obj;
		// elementData[size] = obj;
		// size++;
		return true;
	}
	
	@Override
	public boolean add(int index, Object obj) {
		if (obj == null || index < 0 || index > size)
			return false;
		
		if (size == index) {
			add(obj);
			return true;
		}
		
		if (capacity == size) {
			allocateMemory();
		}
		
		for (int i = size; i > index; i--) {
			elementData[i] = elementData[i - 1];
		}
		
		elementData[index] = obj;
		size++;
		
		return true;
	}
	
	@Override
	public Object get(int index) {
		if (index < 0 || index >= size)
			return false;
		return elementData[index];
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public int indexOf(Object obj) {
		if (obj == null)
			return -1;
		for (int i = 0; i < size; i++) {
			//if (elementData[i].equals(obj))
			if (obj.equals(elementData[i]))
				return i;
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(Object obj) {
		if (obj == null)
			return -1;
		for (int i = size - 1; i >= 0; i--) {
			if (obj.equals(elementData[i]))
				return i;
		}
		return -1;
	}
	
	@Override
	public boolean contains(Object obj) {
		if (obj == null)
			return false;
		for (int i = 0; i < size; i++) {
			if (obj.equals(elementData[i]))
				return true;
		}
		return false;
	}
	
	@Override
	public int lenght() {
		return capacity;
	}
	
	@Override
	public boolean isEmpty() {
		return size == 0;
	}
	
	@Override
	public Object[] toArray() {
		Object[] res = new Object[size];
		for (int i = 0; i < size; i++) {
			res[i] = elementData[i];
		}
		return res;
	}
	
	@Override
	public Object set(int index, Object obj) {
		if (index < 0 || index >= size || obj == null)
			return null;
		Object tmpObj = elementData[index];
		elementData[index] = obj;
		return tmpObj;
	}
	
	@Override
	public boolean remove(Object obj) {
		if (obj == null)
			return false;
		
		boolean isRemoved = false;
		for (int i = 0; i < size; i++) {
			if (obj.equals(elementData[i])) {
				elementData[i] = null;
				isRemoved = true;
			}
		}
		
		if (!isRemoved)
			return false;
		
		int numberOfRemovedElements = 0;
		for (int i = 0; i < size; i++) {
			if (elementData[i] == null) {
				numberOfRemovedElements++;
			} else {
				elementData[i - numberOfRemovedElements] = elementData[i];
			}
		}
		size -= numberOfRemovedElements;
		return true;
	}
	
	@Override
	public Object remove(int index) {
		if (index < 0 || index >= size)
			return null;
		
		Object tmpObj = elementData[index];
		
		for (int i = index; i < size - 1; i++) {
			elementData[i] = elementData[i + 1];
		}
		
		elementData[--size] = null;
		return tmpObj;
	}
	
	@Override
	public void clear() {
		for (int i = 0; i < size; i++) {
			elementData[i] = null;
		}
		size = 0;
	}
	
	@Override
	public boolean removeAll(DinamicArray array) {
		if (array == null || array.isEmpty())
			return false;
		
		boolean isRemoved = false;
		for (int i = 0; i < size; i++) {
			if (array.contains(elementData[i])) {
				elementData[i] = null;
				isRemoved = true;
			}
		}
		
		if (!isRemoved)
			return false;
		
		int numberOfRemovedElements = 0;
		for (int i = 0; i < size; i++) {
			if (elementData[i] == null) {
				numberOfRemovedElements++;
			} else {
				elementData[i - numberOfRemovedElements] = elementData[i];
			}
		}
		size -= numberOfRemovedElements;
		return true;
	}
	
	@Override
	public boolean retainAll(DinamicArray array) {
		if (array == null || array.isEmpty())
			return false;
		
		boolean isRemoved = false;
		for (int i = 0; i < size; i++) {
			if (!array.contains(elementData[i])) {
				elementData[i] = null;
				isRemoved = true;
			}
		}
		
		if (!isRemoved)
			return false;
		
		int numberOfRemovedElements = 0;
		for (int i = 0; i < size; i++) {
			if (elementData[i] == null) {
				numberOfRemovedElements++;
			} else {
				elementData[i - numberOfRemovedElements] = elementData[i];
			}
		}
		size -= numberOfRemovedElements;
		return true;
	}
	
	// Private *********************************************************
	private void allocateMemory() {
		Object[] elementDataTmp = 
				new Object[capacity * EXSTENSIBILITY];
		
		for (int i = 0; i < size; i++) {
			elementDataTmp[i] = elementData[i];
		}
		
		capacity = elementDataTmp.length;
		//capacity = capacity * EXSTENSIBILITY;
		elementData = elementDataTmp;
	}

	
}